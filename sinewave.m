function y = sinewave(x)
    T=2*pi;
    phi=0;
    A=1;
    y = A * sin(((2*pi*x)/T)+phi) ;

