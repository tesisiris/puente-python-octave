Necesitas python y oct2py instalados, así como un `octave` disponible.

Para lanzar el servidor:

	- Bajo Linux, usa 		
		FLASK_APP="test.py" flask run

	- Bajo Windows 10 PowerShell, usa
		$Env:FLASK_APP = "test.py"
		flask run

Para acceder al servidor, en su configuración por defecto:

    http://127.0.0.1:5000/x?a=t.m&b=100&c=20
    
    (llama al script `t.m` con argumentos `100` y `20`
    
En Windows 10, 
	- instala Octave https://ftpmirror.gnu.org/octave/windows/octave-5.2.0_1-w64-installer.exe
	- instala Python 3.8 de https://www.python.org/downloads/windows/
	- instala flask, oct2py vía
		py -m pip install oct2py
		py -m pip install flask

	- ruta de Octave vía 
	$Env:OCTAVE_EXECUTABLE = "C:\Octave\Octave-5.2.0\mingw64\bin\octave-cli.exe"