function [y] = sincwave1(x, amplitude)
    T=2*pi;
    phi=0;
    if (x==0)
    y=amplitude*0.667;
    else
    y = amplitude*abs((2*sin(((2*pi*x)/(3*T))+phi))/x);
    endif
