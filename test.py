from flask import Flask, escape, request
from oct2py import octave
from re import sub
import threading
import time

sem = threading.Semaphore()
app = Flask(__name__)

@app.after_request
def add_header(response):
	# response.cache_control.max_age = 0 // always regenerate
	return response

@app.route('/f/<fname>')
def compute(fname):
	xmin = float(request.args.get("xmin", "1"))
	xmax = float(request.args.get("xmax", "10"))
	n = int(request.args.get("n", "10"))
	 #transition = float(request.args.get("transition", "1"))
	 #amplitude=int(request.args.get("amplitude", "1"))
	a = float(request.args.get("a", "1"))
	b = float(request.args.get("b", "1"))
	sem.acquire() # protect single instance of octave, which is not thread-safe
	# result = octave.feval("sampler.m", xmin, xmax, n, transition, amplitude, fname, a, b)
	result = octave.feval("sampler.m", xmin, xmax, n, fname, a, b)
	sem.release()
	ret = sub(r'[\[\]]*', '', f'{escape(result)}')
	print(ret)
	return ret
