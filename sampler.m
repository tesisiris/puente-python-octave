function [y] = sampler(xmin, xmax, n, fname, a, b)

    # see https://octave.org/doc/v5.2.0/Calling-a-Function-by-its-Name.html

    step = (xmax-xmin+1)/n;
    x = xmin;
    for i = 1:n
        y(i) = feval(fname, x, a, b);
        x = x + step;
    endfor
