function y = square_signal(x, transition, v0=1, v1=0)
    if (x < transition)
        y = v0
    else
        y = v1
    endif
