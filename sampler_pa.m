# Debe generar una matriz en vez de un vector, con los valores aleatorios.


function [y] = sampler_pa(xmin, xmax, numValues, numGraphs, fname, iname, a, b)

    # see https://octave.org/doc/v5.2.0/Calling-a-Function-by-its-Name.html

    step = (xmax-xmin+1)/numValues;

    for j = 1:numGraphs
	    cosas = feval(iname, a, b);
        x = xmin;
        for i = 1:numValues
            y(j,i) = feval(fname, cosas, x, a, b);
      	    x = x + step;
        endfor
    endfor


