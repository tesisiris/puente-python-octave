function [y] = sincwave(x, amplitude)
    T=2*pi;
    phi=0;
    if (x==0)
    y=amplitude*1;
    else
    y = amplitude*abs((sin(((2*pi*x)/T)+phi)+sin(((2*pi*(x-10))/6*T)+phi)+sin(((2*pi*(x+10))/6*T)+phi))/x);
    endif
