from flask import Flask, escape, request
from oct2py import octave
from re import sub
import threading
import time

sem = threading.Semaphore()
app = Flask(__name__)

@app.after_request
def add_header(response):
    # response.cache_control.max_age = 0 // always regenerate
    return response

@app.route('/f/<fname>/<iname>')
def compute(fname, iname):
    xmin = float(request.args.get("xmin", "1"))
    xmax = float(request.args.get("xmax", "10"))
    numValues = float(request.args.get("numValues", "10"))
    numGraphs = float(request.args.get("numGraphs", "3"))
    a = float(request.args.get("a", "1"))
    b = float(request.args.get("b", "1"))
    octave.eval('pkg load statistics')
    sem.acquire() # protect single instance of octave, which is not thread-safe
    result = octave.feval("sampler_pa.m", xmin, xmax, numValues, numGraphs, fname, iname, a, b)
    sem.release()
    resultToString = ' '.join(map(str, result))
    ret = sub(r'[\[\]]*', '', resultToString)
    print(ret)
    return ret